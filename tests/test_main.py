'''Tests for src/git_cu/main.py'''

from pathlib import PurePath

import pytest

from git_cu import main

# tests written in the style of the official pytest examples trigger pylint
# warnings, disable them

# pylint: disable=no-self-use,too-few-public-methods,unused-argument,redefined-outer-name


@pytest.fixture
def git_cu_dir(monkeypatch):
    '''mock os.environ['GIT_CU_DIR']'''
    monkeypatch.setenv('GIT_CU_DIR', '/home/user/Code')


@pytest.fixture
def no_git_cu_dir(monkeypatch):
    '''mock os.environ['GIT_CU_DIR']'''
    monkeypatch.delenv('GIT_CU_DIR', raising=False)


@pytest.fixture
def mock_execvp(mocker):
    '''mock os.execvp'''
    return mocker.patch('os.execvp')


class TestParseArgs:
    '''Tests for parse_args'''
    def test_pass_args_to_git(self):
        '''Test passing args to git-clone'''
        parsed = main.parse_args(['url', '--bare'])
        assert parsed.url == 'url'
        assert parsed.args == ['--bare']


class TestGitCu:
    '''Tests for git_cu'''
    def test_no_git_cu_dir(self, no_git_cu_dir, mock_execvp):
        '''Test real run with GIT_CU_DIR not set'''
        main.git_cu('https://gitlab.com/3point2/git-cu.git', [], False)
        mock_execvp.assert_called_once_with('git', [
            'git', 'clone', '--', 'https://gitlab.com/3point2/git-cu.git',
            'gitlab.com/3point2/git-cu'
        ])

    def test_git_cu_dir(self, git_cu_dir, mock_execvp):
        '''Test real run with GIT_CU_DIR set'''
        main.git_cu('https://gitlab.com/3point2/git-cu.git', [], False)
        mock_execvp.assert_called_once_with('git', [
            'git', 'clone', '--', 'https://gitlab.com/3point2/git-cu.git',
            '/home/user/Code/gitlab.com/3point2/git-cu'
        ])

    def test_double_dot(self, git_cu_dir, mock_execvp):
        '''Test real run with .. in path'''
        with pytest.raises(SystemExit):
            main.git_cu('../../repo', [], False)
        mock_execvp.assert_not_called()

    def test_no_url(self, git_cu_dir, mock_execvp):
        '''Test real run a path instead of a URL'''
        with pytest.raises(SystemExit):
            main.git_cu('/path/to/repo', [], False)
        mock_execvp.assert_not_called()

    def test_malformed_url(self, git_cu_dir, mock_execvp):
        '''Test real run a path instead of a URL'''
        with pytest.raises(SystemExit):
            main.git_cu('http://host[/path/to/repo', [], False)
        mock_execvp.assert_not_called()

    def test_git_args(self, no_git_cu_dir, mock_execvp):
        '''Test real run with GIT_CU_DIR not set'''
        main.git_cu('https://gitlab.com/3point2/git-cu.git', ['--bare'], False)
        mock_execvp.assert_called_once_with('git', [
            'git', 'clone', '--bare', '--',
            'https://gitlab.com/3point2/git-cu.git',
            'gitlab.com/3point2/git-cu'
        ])

    def test_dry_run(self, no_git_cu_dir, mock_execvp):
        '''Test dry run with GIT_CU_DIR not set'''
        main.git_cu('https://gitlab.com/3point2/git-cu.git', [], True)
        mock_execvp.assert_not_called()

    def git_execution_error(self, mock_execvp):
        '''Test exit without crash if git not found'''
        # FileNotFoundError is a subclass of OsError
        mock_execvp.side_effect = FileNotFoundError
        with pytest.raises(SystemExit):
            main.git_cu('https://gitlab.com/3point2/git-cu.git', [], True)


class TestAssemblePath:
    '''Tests for assemble_path'''
    def test_assemble_abs_path(self):
        '''Test removing unnecessary path components'''
        result = main.assemble_path('test.com', '/absolute/path.git')
        assert result == PurePath('test.com/absolute/path')

    def test_passthrough(self):
        '''Test already suitable path is unmodified'''
        result = main.assemble_path('test.com', 'clean/path')
        assert result == PurePath('test.com/clean/path')


class TestIsUrl:
    '''Tests for is_url'''
    def test_url(self):
        '''Test URL'''
        assert main.is_url('https://gitlab.com/3point2/git-cu.git')

    def test_scp(self):
        '''Test scp-style URL'''
        assert not main.is_url('git@gitlab.com:3point2/git-cu.git')

    def test_path(self):
        '''Test local path'''
        assert not main.is_url('/path/to/repo')


class TestIsScp:
    '''Tests for is_scp'''
    def test_scp(self):
        '''Test scp-style URL'''
        assert main.is_scp('git@gitlab.com:3point2/git-cu.git')

    def test_path(self):
        '''Test local path'''
        assert not main.is_scp('/path/to/repo')


class TestParse:
    '''Tests for parse'''
    def test_parse(self):
        '''Test URL'''
        result = main.parse('https://gitlab.com/3point2/git-cu.git')
        assert result == PurePath('gitlab.com', '3point2', 'git-cu')

    def test_parse_ssh(self):
        '''Test URL with username and port'''
        result = main.parse('ssh://git@domain.com:7999/path/repo.git')
        assert result == PurePath('domain.com', 'path', 'repo')

    def test_parse_file(self):
        '''Test file URL'''
        result = main.parse('file:///path/to/local/repo')
        assert result == PurePath('path', 'to', 'local', 'repo')


class TestParseScp:
    '''Tests for parse_scp'''
    def test_parse_scp(self):
        '''Test scp-style URL'''
        result = main.parse_scp('git@gitlab.com:3point2/git-cu.git')
        assert result == PurePath('gitlab.com', '3point2', 'git-cu')

    def test_parse_scp_nouser(self):
        '''Test scp-style URL with no username'''
        result = main.parse_scp('gitlab.com:3point2/git-cu.git')
        assert result == PurePath('gitlab.com', '3point2', 'git-cu')


class TestGetDestDir:
    '''Tests for get_dest_dir'''
    def test_url(self):
        '''Test a URL'''
        result = main.get_dest_dir('https://gitlab.com/3point2/git-cu.git')
        assert result == PurePath('gitlab.com', '3point2', 'git-cu')

    def test_scp(self):
        '''Test an scp-style url'''
        result = main.get_dest_dir('git@gitlab.com:3point2/git-cu.git')
        assert result == PurePath('gitlab.com', '3point2', 'git-cu')

    def test_local(self):
        '''Test a local path'''
        with pytest.raises(main.UrlParseError):
            main.get_dest_dir('/path/to/repo')
